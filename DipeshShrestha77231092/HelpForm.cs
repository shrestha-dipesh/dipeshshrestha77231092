﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the help form that contains the instructions and command guidelines for Component 1.
    /// </summary>
    public partial class HelpForm : Form
    {
        Graphics graphics;
        Pen pen;
        readonly SolidBrush brush = new SolidBrush(Color.Red);

        /// <summary>
        /// Default constructor to initialize the component.
        /// </summary>
        public HelpForm()
        {
            InitializeComponent();
        }

        private void circlePicture_Paint(object sender, PaintEventArgs e)
        {
            graphics = e.Graphics;
            pen = new Pen(Color.White);

            graphics.DrawRectangle(pen, 45 - 90 / 2, 45 - 90/ 2, 90, 90);
            graphics.FillEllipse(brush, 45 - 2, 45 - 2, 4, 4);
            graphics.DrawEllipse(pen, 45 - 30, 45 - 30, 60, 60);
        }

        private void rectanglePicture_Paint(object sender, PaintEventArgs e)
        {
            graphics = e.Graphics;
            pen = new Pen(Color.White);

            graphics.DrawRectangle(pen, 45 - 90 / 2, 45 - 90 / 2, 90, 90);
            graphics.FillEllipse(brush, 45 - 2, 45 - 2, 4, 4);
            graphics.DrawRectangle(pen, 45 - 60 / 2, 45 - 50 /2, 60, 50);
        }

        private void trianglePicture_Paint(object sender, PaintEventArgs e)
        {
            graphics = e.Graphics;
            pen = new Pen(Color.White);

            graphics.DrawRectangle(pen, 45 - 90 / 2, 45 - 90 / 2, 90, 90);
            graphics.FillEllipse(brush, 45 - 2, 45 - 2, 4, 4);
            graphics.DrawPolygon(pen, new Point[] { new Point(45, 45 - 60 / 2), new Point(45 + 60 / 2, 45 + 60 / 2), new Point(45 - 60 / 2, 45 + 60 / 2) });
        }

        private void penPicture_Paint(object sender, PaintEventArgs e)
        {
            graphics = e.Graphics;
            pen = new Pen(Color.White);

            graphics.DrawRectangle(pen, 45 - 90 / 2, 45 - 90 / 2, 90, 90);
            graphics.FillEllipse(brush, 45 - 2, 45 - 2, 4, 4);

            pen = new Pen(Color.Red);
            graphics.DrawEllipse(pen, 45 - 30, 45 - 30, 60, 60);
        }

        private void fillPicture_Paint(object sender, PaintEventArgs e)
        {
            graphics = e.Graphics;
            pen = new Pen(Color.White);

            graphics.DrawRectangle(pen, 45 - 90 / 2, 45 - 90 / 2, 90, 90);
            graphics.FillEllipse(brush, 45 - 2, 45 - 2, 4, 4);
            graphics.FillRectangle(brush, 45 - 60 / 2, 45 - 50 / 2, 60, 50);
        }

        private void drawtoPicture_Paint(object sender, PaintEventArgs e)
        {
            graphics = e.Graphics;
            pen = new Pen(Color.White);

            graphics.DrawRectangle(pen, 45 - 90 / 2, 45 - 90 / 2, 90, 90);
            graphics.FillEllipse(brush, 35 - 2, 35 - 2, 4, 4);
            graphics.DrawLine(pen, 35, 35, 55, 55);
        }

        private void movetoPicture_Paint(object sender, PaintEventArgs e)
        {
            graphics = e.Graphics;
            pen = new Pen(Color.White);

            graphics.DrawRectangle(pen, 45 - 90 / 2, 45 - 90 / 2, 90, 90);
            graphics.FillEllipse(new SolidBrush(Color.Gray), 35 - 2, 35 - 2, 4, 4);
            graphics.FillEllipse(brush, 55 - 2, 55 - 2, 4, 4);
        }
    }
}