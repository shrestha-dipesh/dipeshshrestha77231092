﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the abstract class of the Shape to implement the possible abstract methods.
    /// </summary>
    public abstract class Shape : Shapes
    {
        /// <summary>
        /// Color of the shape.
        /// </summary>
        protected Color colour;

        /// <summary>
        /// Center point of the shape.
        /// </summary>
        protected int x_coordinate, y_coordinate;

        /// <summary>
        /// Fill flag of the shape.
        /// </summary>
        protected bool fillOn;

        /// <summary>
        /// Flash flag of the shape.
        /// </summary>
        protected static bool flashOn;

        /// <summary>
        /// Thread to implement flashing colors.
        /// </summary>
        protected Thread newThread;

        /// <summary>
        /// Flags required for thread.
        /// </summary>
        protected bool flag, isRunning, threadStarted;

        /// <summary>
        /// List of threads.
        /// </summary>
        protected static List<Thread> threadList = new List<Thread>();

        /// <summary>
        /// Colors required for flashing.
        /// </summary>
        public string[] flashColor = new string[2];

        /// <summary>
        /// fillOn Property
        /// </summary>
        public bool FillOn
        {
            get { return fillOn; }
            set { fillOn = value; }
        }

        /// <summary>
        /// flashOn Property
        /// </summary>
        public static bool FlashOn
        {
            get { return flashOn; }
            set { flashOn = value; }
        }

        /// <summary>
        /// Default constructor to set the color and center point of the shape.
        /// </summary>
        public Shape()
        {
            colour = Color.White;
            x_coordinate = y_coordinate = 100;
        }

        /// <summary>
        /// Parameterized constructor to set the provided parameters to their respective attributes.
        /// </summary>
        /// <param name="colour">Color of the shape</param>
        /// <param name="x_coordinate">X-Coordinate of the center point</param>
        /// <param name="y_coordinate">Y-Coordinate of the center point</param>
        public Shape(Color colour, int x_coordinate, int y_coordinate)
        {
            this.colour = colour;
            this.x_coordinate = x_coordinate;
            this.y_coordinate = y_coordinate;
        }

        /// <summary>
        /// Implemented abstract method to draw the shape on the graphics.
        /// </summary>
        /// <param name="graphics">Reference to the graphics where the shape is to be drawn</param>
        public abstract void draw(Graphics graphics);

        /// <summary>
        /// Implemented abstract method to set the required parameters.
        /// </summary>
        /// <param name="colour">Color of the shape</param>
        /// <param name="list">Array of parameters containing coordinates of center point</param>
        public virtual void set(Color colour, params int[] list)
        {
            this.colour = colour;
            x_coordinate = list[0];
            y_coordinate = list[1];
        }

        /// <summary>
        /// Method to add thread to the list.
        /// </summary>
        /// <param name="thread">Object of thread that is being executed</param>
        public static void addThread(Thread thread)
        {
            threadList.Add(thread);
        }

        /// <summary>
        /// Method to return the list of threads.
        /// </summary>
        /// <returns>Object reference of the threads</returns>
        public static List<Thread> getThreads()
        {
            return threadList;
        }

        /// <summary>
        /// Method to set the flashing colors.
        /// </summary>
        /// <param name="firstColor">First color of the flashing color</param>
        /// <param name="secondColor">Second color of the flashing color</param>
        public void setFlashColor(string firstColor, string secondColor)
        {
            flashColor[0] = firstColor;
            flashColor[1] = secondColor;
        }

        /// <summary>
        /// Method to set the color fill on the shape.
        /// </summary>
        /// <param name="fillOn">Flag for the fill attribute</param>
        public void setFill(bool fillOn)
        {
            this.fillOn = fillOn;
        }

        /// <summary>
        /// Method to continuously flash the alterate colors.
        /// </summary>
        /// <param name="graphics">Reference to the graphics where the shape is to be drawn</param>
        public void threadMethod(Graphics graphics)
        {
            while (true)
            {
                while (isRunning)
                {
                    if (flag == false)
                    {
                        colour = Color.FromName(flashColor[0]);
                        draw(graphics);
                        flag = true;
                    }
                    else
                    {
                        colour = Color.FromName(flashColor[1]);
                        draw(graphics);
                        flag = false;
                    }
                    Thread.Sleep(500);
                }
            }
        }

        /// <summary>
        /// Overridden method to describe the object of the class.
        /// </summary>
        /// <returns>Coordinates of center point of the shape</returns>
        public override string ToString()
        {
            return base.ToString() + " " + x_coordinate + "," + y_coordinate + " : ";
        }
    }
}