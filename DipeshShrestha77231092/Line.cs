﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the line shape.
    /// </summary>
    public class Line : Shape
    {
        private int x_coords, y_coords;

        /// <summary>
        /// Default constructor to set the end coordinates of the line.
        /// </summary>
        public Line() : base()
        {
            x_coords = 100;
            y_coords = 100;
        }

        /// <summary>
        /// Parameterized constructor to set the provided parameters to their respective attributes.
        /// </summary>
        /// <param name="colour">Color of the line</param>
        /// <param name="x_coordinate">X-Coordinate of the center point</param>
        /// <param name="y_coordinate">Y-Coordinate of the center point</param>
        /// <param name="x_coords">X-Coordinate of the end point</param>
        /// <param name="y_coords">Y-Coordinate of the end point</param>
        public Line(Color colour, int x_coordinate, int y_coordinate, int x_coords, int y_coords) : base(colour, x_coordinate, y_coordinate)
        {
            this.x_coords = x_coords;
            this.y_coords = y_coords;
        }

        /// <summary>
        /// Overridden method of Shape to set the color, center point and end point of the line.
        /// </summary>
        /// <param name="colour">Color of the line</param>
        /// <param name="list">Array of parameters containing center point and end point</param>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            x_coords = list[2];
            y_coords = list[3];
        }

        /// <summary>
        /// Overridden method of Shape to draw the line on the graphics.
        /// </summary>
        /// <param name="graphics"></param>
        public override void draw(Graphics graphics)
        {
            //Start a thread that will flash alternate shape color
            if (FlashOn && !threadStarted)
            {
                newThread = new Thread(() => threadMethod(graphics));
                newThread.Start();
                addThread(newThread);
                isRunning = true;
                threadStarted = true;
            }
            else
            {
                Pen pen = new Pen(colour, 2);
                graphics.DrawLine(pen, x_coordinate, y_coordinate, x_coords, y_coords);
                pen.Dispose();
            }  
        }
    }
}