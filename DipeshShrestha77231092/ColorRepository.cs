﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the repository that contains the list of supported colors.
    /// </summary>
    public class ColorRepository : Container
    {
        /// <summary>
        /// List of supported Colors.
        /// </summary>
        public readonly string[] supportedColours = { "red", "green", "blue", "white", "black", "yellow", "orange", "brown", "pink", "purple", "gray" };

        /// <summary>
        /// Implemented Method that instantiates the ColorIterator class.
        /// </summary>
        /// <returns>Object of ColorIterator class</returns>
        public Iterator getIterator()
        {
            return new ColorIterator();
        }

        /// <summary>
        /// Represents class that returns individual color from the ColorRepository.
        /// </summary>
        private class ColorIterator : ColorRepository, Iterator
        {
            int index;

            /// <summary>
            /// Implemented method that checks if the provided index is within the boundary of the list.
            /// </summary>
            /// <returns>True if index within boundary</returns>
            public bool hasNext()
            {
                if (index < supportedColours.Length)
                {
                    return true;
                }
                return false;
            }

            /// <summary>
            /// Implemented method that returns the next color from the list.
            /// </summary>
            /// <returns>Object of the supported color</returns>
            public object next()
            {
                if (hasNext())
                {
                    return supportedColours[index++];
                }
                return null;
            }
        }
    }
}