﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represent the form GUI.
    /// </summary>
    public partial class GuiForm : Form
    {
        ShapeFactory factory = new ShapeFactory();
        Shape shape;
        
        /// <summary>
        /// Default constructor to initialize the component.
        /// </summary>
        public GuiForm()
        {
            InitializeComponent();
        }

        private void programBox_Enter(object sender, EventArgs e)
        {
            if (programBox.Text == "Type your program code here")
            {
                programBox.Text = "";
                programBox.ForeColor = Color.White;
            }
        }

        private void programBox_Leave(object sender, EventArgs e)
        {
            if (programBox.Text == "")
            {
                programBox.Text = "Type your program code here";
                programBox.ForeColor = Color.Gray;
            }
        }

        private void commandBox_Enter(object sender, EventArgs e)
        {
            if (commandBox.Text == "Type your command here")
            {
                commandBox.Text = "";
                commandBox.ForeColor = Color.White;
            }
        }

        private void commandBox_Leave(object sender, EventArgs e)
        {
            if (commandBox.Text == "")
            {
                commandBox.Text = "Type your command here";
                commandBox.ForeColor = Color.Gray;
            }
        }

        private void outputBox_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            SolidBrush brush = new SolidBrush(Color.Red);

            graphics.FillEllipse(brush, CenterPoint.X_Coordinate - 4, CenterPoint.Y_Coordinate - 4, 8, 8);

            if (shape != null)
                shape.draw(graphics);
        }

        /// <summary>
        /// Method to read, parse and execute the code.
        /// </summary>
        /// <param name="inputProgram">Input text from the text box</param>
        public void runProgram(string inputProgram)
        {
            statusBox.Text = "";
            statusBox.ForeColor = Color.Crimson;
            Command.HasError = false;

            try
            {
                if (inputProgram == "" || inputProgram == "Type your program code here")
                {
                    Command.HasError = true;
                    throw new Exception("No command found");
                }

                //Instantiate the command parser class to parse the input text
                CommandParser parser = new CommandParser();
                Command[] commandList = parser.parse(inputProgram, statusBox);

                foreach (Command command in commandList)
                {

                    try
                    {
                        //Call the getShape method of ShapeFactory to return the appropriate shape object
                        shape = factory.getShape(command.CommandName);
                    }
                    catch (ArgumentException) { }

                    string errorMessage = command.debug();
                    if (errorMessage != null)
                    {
                        statusBox.Text = errorMessage;
                        break;
                    }

                    try
                    {
                        command.execute(shape, outputBox);
                    }
                    catch (FormatException)
                    {
                        Command.HasError = true;
                        statusBox.Text = "Error: Invalid parameter in \"" + command.CommandName + "\" ";
                    }
                }
            }
            catch (Exception ex)
            {
                Command.HasError = true;
                statusBox.Text = "Error: " + ex.Message;
            }

            if (Command.HasError == false)
            {
                statusBox.ForeColor = Color.FromArgb(0, 184, 148);
                statusBox.Text = "Code compiled successfully !!!";
            }
        }

        private void runButton_Click(object sender, EventArgs e)
        {
            ConditionalCommand.OutputBox = outputBox;
            ConditionalCommand.StatusBox = statusBox;
            IteratorCommand.OutputBox = outputBox;
            IteratorCommand.StatusBox = statusBox;
            MethodCommand.OutputBox = outputBox;
            MethodCommand.StatusBox = statusBox;
            runProgram(programBox.Text);
        }

        /// <summary>
        /// Method to read and parse the input text to check for errors without execution.
        /// </summary>
        private void debugButton_Click(object sender, EventArgs e)
        {
            ConditionalCommand.OutputBox = new PictureBox();
            ConditionalCommand.StatusBox = statusBox;
            IteratorCommand.OutputBox = new PictureBox();
            IteratorCommand.StatusBox = statusBox;
            MethodCommand.OutputBox = new PictureBox();
            MethodCommand.StatusBox = statusBox;

            statusBox.Text = "";
            statusBox.ForeColor = Color.Crimson;
            Command.HasError = false;
            string inputProgram = programBox.Text;

            try
            {
                if (inputProgram == "" || inputProgram == "Type your program code here")
                {
                    Command.HasError = true;
                    throw new Exception("No command found");
                }

                //Instantiate the command parser class to parse the input text
                CommandParser parser = new CommandParser();
                Command[] commandList = parser.parse(inputProgram, statusBox);

                foreach (Command command in commandList)
                {
                    try
                    {
                        //Call the getShape method of ShapeFactory to return the appropriate shape object
                        shape = factory.getShape(command.CommandName);
                    }
                    catch (ArgumentException) { }

                    string errorMessage = command.debug();
                    if (errorMessage != null)
                    {
                        statusBox.Text = errorMessage;
                        break;
                    }

                    try
                    {
                        command.execute(shape, new PictureBox());
                    }
                    catch (FormatException)
                    {
                        Command.HasError = true;
                        statusBox.Text = "Error: Invalid parameter in \"" + command.CommandName + "\" ";
                    }
                }
            }
            catch (Exception ex)
            {
                Command.HasError = true;
                statusBox.Text = "Error: " + ex.Message;
            }

            if (Command.HasError == false)
            {
                statusBox.ForeColor = Color.FromArgb(0, 184, 148);
                statusBox.Text = "Code compiled successfully !!!";
            }
        }

        private void loadMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Text File | *.txt";

            //Display the open file dialog box
            if (open.ShowDialog() == DialogResult.OK)
            {
                StreamReader streamReader = new StreamReader(open.OpenFile());
                programBox.Text = null;
                programBox.ForeColor = Color.White;
                
                //Loop through the text file and store each line in the text box
                do
                {
                    string line = streamReader.ReadLine();
                    if (line == null)
                        break;
                    programBox.Text += line + Environment.NewLine;
                } while (true);

                streamReader.Close();
            }
        }

        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "Project.txt";
            save.Filter = "Text File | *.txt";

            //Display the save file dialog box
            if (save.ShowDialog() == DialogResult.OK)
            {
                //Store the content of text box in the file
                StreamWriter streamWriter = new StreamWriter(save.OpenFile());
                streamWriter.WriteLine(programBox.Text);
                streamWriter.Close();
            }
        }

        private void exitMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// Method to run single line commands.
        /// </summary>
        private void commandBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (commandBox.Text.ToLower() == "run")
                {
                    runProgram(programBox.Text);
                }
                else if (commandBox.Text.ToLower() == "reset")
                {
                    //Terminates all the threads
                    foreach (Thread thread in Shape.getThreads())
                    {
                        thread.Abort();
                    }

                    //Clears the drawing area
                    outputBox.CreateGraphics().Clear(outputBox.BackColor);
                    CenterPoint.moveCenterPoint(outputBox, 0, 0);

                    //Clear the program box
                    programBox.Text = "Type your program code here";
                    programBox.ForeColor = Color.Gray;
                    statusBox.Text = "";
                }
                else if (commandBox.Text.ToLower() == "clear")
                {
                    //Terminates all the threads
                    foreach(Thread thread in Shape.getThreads())
                    {
                        thread.Abort();
                    }

                    //Clear the drawing area
                    outputBox.CreateGraphics().Clear(outputBox.BackColor);
                    CenterPoint.moveCenterPoint(outputBox, CenterPoint.X_Coordinate, CenterPoint.Y_Coordinate);
                }
                else
                {
                    runProgram(commandBox.Text);
                }
            }
        }

        private void component1HelpMenu_Click(object sender, EventArgs e)
        {
            HelpForm helpForm = new HelpForm();
            helpForm.Show();
        }

        private void component2HelpMenu_Click(object sender, EventArgs e)
        {
            AdvancedHelpForm advancedHelpForm = new AdvancedHelpForm();
            advancedHelpForm.Show();
        }
    }
}