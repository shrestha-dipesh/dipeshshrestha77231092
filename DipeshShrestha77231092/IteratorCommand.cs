﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents iterative commands that is stored inside while statement.
    /// </summary>
    public class IteratorCommand
    {
        private List<Command> commandList;
        private static PictureBox outputBox;
        private static Label statusBox;
        private int lineNumber;
        ShapeFactory factory = new ShapeFactory();
        Shape shape;

        /// <summary>
        /// outputBox Property
        /// </summary>
        public static PictureBox OutputBox
        {
            get { return outputBox; }
            set { outputBox = value; }
        }

        /// <summary>
        /// statusBox Property
        /// </summary>
        public static Label StatusBox
        {
            get { return statusBox; }
            set { statusBox = value; }
        }

        /// <summary>
        /// Parameterized constructor to set the line number of the command and instantiate the list of commands.
        /// </summary>
        /// <param name="lineNumber">Line number of the command</param>
        public IteratorCommand(int lineNumber)
        {
            this.lineNumber = lineNumber;
            commandList = new List<Command>();
        }

        /// <summary>
        /// Method to add the commands to the list.
        /// </summary>
        /// <param name="command">Object of command to be executed</param>
        public void add(Command command)
        {
            bool alreadyExists = commandList.Contains(command);
            if (!alreadyExists)
            {
                commandList.Add(command);
            }
        }

        /// <summary>
        /// Method to execute all the commands in the command list.
        /// </summary>
        public void executeCommand()
        {
            foreach (Command command in commandList)
            {
                try
                {
                    //Call the getShape method of ShapeFactory to return the appropriate shape object
                    shape = factory.getShape(command.CommandName);
                }
                catch (ArgumentException) { }

                string errorMessage = command.debug();
                if (errorMessage != null)
                {
                    statusBox.Text = errorMessage;
                    break;
                }

                try
                {
                    command.execute(shape, outputBox);
                }
                catch (FormatException)
                {
                    Command.HasError = true;
                    statusBox.Text = "Error: Invalid parameter in \"" + command.CommandName + "\" ";
                }
            }
        }

        /// <summary>
        /// Method to check if the expression is valid and condition is satisfied.
        /// </summary>
        /// <param name="condition">Expression of the condition</param>
        /// <returns>True if condition is satisfied</returns>
        public bool checkCondition(string condition)
        {
            string delimiter = "";
            bool hasVariable = false;
            bool conditionValid = false;
            string[] variables;

            //Identifies the operator delimiter and splits the operands
            variables = condition.Split(new string[] { "==" }, 2, StringSplitOptions.None).Select(variable => variable.Trim()).ToArray();
            if (variables.Length == 2)
            {
                delimiter = "==";
            }
            else
            {
                variables = condition.Split(new string[] { "<=" }, 2, StringSplitOptions.None).Select(variable => variable.Trim()).ToArray();
                if (variables.Length == 2)
                {
                    delimiter = "<=";
                }
                else
                {
                    variables = condition.Split(new string[] { ">=" }, 2, StringSplitOptions.None).Select(variable => variable.Trim()).ToArray();
                    if (variables.Length == 2)
                    {
                        delimiter = ">=";
                    }
                    else
                    {
                        variables = condition.Split(new string[] { "<" }, 2, StringSplitOptions.None).Select(variable => variable.Trim()).ToArray();
                        if (variables.Length == 2)
                        {
                            delimiter = "<";
                        }
                        else
                        {
                            variables = condition.Split(new string[] { ">" }, 2, StringSplitOptions.None).Select(variable => variable.Trim()).ToArray();
                            if (variables.Length == 2)
                            {
                                delimiter = ">";
                            }
                            else
                            {
                                variables = condition.Split(new string[] { "!=" }, 2, StringSplitOptions.None).Select(variable => variable.Trim()).ToArray();
                                if (variables.Length == 2)
                                {
                                    delimiter = "!=";
                                }
                            }
                        }
                    }
                }
            }

            if (delimiter == "")
            {
                throw new Exception("Undefined operator at line " + lineNumber);
            }


            if (variables.Length == 2)
            {
                //Loops through the variable list and sets the appropriate value of variables
                for (int index = 0; index < variables.Length; index++)
                {
                    foreach (Variable variable in Variable.getVariables())
                    {
                        if (variables[index] == variable.Name)
                        {
                            variables[index] = variable.Values;
                            hasVariable = true;
                            break;
                        }
                        else
                        {
                            if (!int.TryParse(variables[index], out _))
                            {
                                hasVariable = false;
                            }
                        }
                    }

                    if (!hasVariable)
                    {
                        conditionValid = false;

                        //Checks for unnecessary regular expression on the operator and operands
                        Regex regex = new Regex("[^A-Za-z0-9]");
                        if (regex.IsMatch(variables[0]) || regex.IsMatch(variables[1]))
                        {

                            throw new Exception("Undefined operator at line " + lineNumber);
                        }
                        else
                        {
                            throw new Exception("Undefined variable at line " + lineNumber);
                        }
                    }
                }

                switch (delimiter)
                {
                    case "==":
                        if (int.Parse(variables[0]) == int.Parse(variables[1]))
                        {
                            conditionValid = true;
                        }
                        break;

                    case ">=":
                        if (int.Parse(variables[0]) >= int.Parse(variables[1]))
                        {
                            conditionValid = true;
                        }
                        break;

                    case "<=":
                        if (int.Parse(variables[0]) <= int.Parse(variables[1]))
                        {
                            conditionValid = true;
                        }
                        break;

                    case ">":
                        if (int.Parse(variables[0]) > int.Parse(variables[1]))
                        {
                            conditionValid = true;
                        }
                        break;

                    case "<":
                        if (int.Parse(variables[0]) < int.Parse(variables[1]))
                        {
                            conditionValid = true;
                        }
                        break;

                    case "!=":
                        if (int.Parse(variables[0]) != int.Parse(variables[1]))
                        {
                            conditionValid = true;
                        }
                        break;
                }
            }
            return conditionValid;
        }
    }
}