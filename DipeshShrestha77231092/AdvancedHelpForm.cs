﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the help form that contains the instructions and command guidelines for Component 2.
    /// </summary>
    public partial class AdvancedHelpForm : Form
    {
        /// <summary>
        /// Default constructor to initialize the component.
        /// </summary>
        public AdvancedHelpForm()
        {
            InitializeComponent();
        }

        private void movetoPicture_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Pen pen = new Pen(Color.White);
            SolidBrush brush = new SolidBrush(Color.Red);

            graphics.DrawRectangle(pen, 45 - 90 / 2, 45 - 90 / 2, 90, 90);
            //polygon 160, 300, 280, 205, 120, 205, 240, 300
            PointF[] points = new PointF[] { new Point(45, 20), new Point(30, 60), new Point(65, 35), new Point(25, 35), new Point(60, 60)};
            graphics.DrawPolygon(pen, points);
            graphics.FillEllipse(brush, 45 - 2, 20 - 2, 4, 4);
        }
    }
}