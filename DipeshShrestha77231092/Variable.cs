﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the variable.
    /// </summary>
    public class Variable
    {
        private string name, values;
        private int variableNumber;
        private static List<Variable> variableList;

        /// <summary>
        /// name Property
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// values Property
        /// </summary>
        public string Values
        {
            get { return values; }
            set { values = value; }
        }

        /// <summary>
        /// variableNumber Property
        /// </summary>
        public int VariableNumber
        {
            get { return variableNumber; }
            set { variableNumber = value; }
        }
        
        /// <summary>
        /// Default constructor to instantiate the variable list.
        /// </summary>
        public Variable()
        {
            variableList = new List<Variable>();
        }

        /// <summary>
        /// Parameterized constructor to set the variable name, value and line number of the variable.
        /// </summary>
        /// <param name="name">Name of the variable</param>
        /// <param name="values">Value of the variable</param>
        /// <param name="variableNumber">Line number of the variable</param>
        public Variable(string name, string values, int variableNumber)
        {
            this.name = name;
            this.values = values;
            this.variableNumber = variableNumber;
            computeValue();
        }

        /// <summary>
        /// Method to add variable to the list using Composite Design Pattern.
        /// </summary>
        /// <param name="variable">Object of the variable</param>
        public static void add(Variable variable)
        {
            bool variableFound = false;
            if (variableList.Count == 0)
            {
                variableList.Add(variable);
            }
            else
            {
                foreach (Variable localVariable in variableList)
                {
                    if (localVariable.name == variable.name)
                    {
                        variableFound = true;
                        throw new Exception("Variable \"" + variable.name + "\" already declared");
                    }
                }
                if (!variableFound)
                {
                    variableList.Add(variable);
                }
            }
        }

        /// <summary>
        /// Method to return the list of variables.
        /// </summary>
        /// <returns>Object reference of the variables.</returns>
        public static List<Variable> getVariables()
        {
            return variableList;
        }

        /// <summary>
        /// Method to perform the calculation of the variables.
        /// </summary>
        public void computeValue()
        {
            char delimiter = ' ';
            string[] data;

            //Checks for unnecessary regular expression on the variable
            Regex regex = new Regex("[^A-Za-z0-9]");
            if (regex.IsMatch(name))
            {
                throw new Exception("Undefined operator at line " + variableNumber);
            }

            //Identifies the operator delimiter and splits the operands
            data = values.Split(new char[] { '+' }, 2).Select(variable => variable.Trim()).ToArray();
            if (data.Length == 2)
            {
                delimiter = '+';
            }
            else
            {
                data = values.Split(new char[] { '-' }, 2).Select(variable => variable.Trim()).ToArray();
                if (data.Length == 2)
                {
                    delimiter = '-';
                }
                else
                {
                    data = values.Split(new char[] { '*' }, 2).Select(variable => variable.Trim()).ToArray();
                    if (data.Length == 2)
                    {
                        delimiter = '*';
                    }
                    else
                    {
                        data = values.Split(new char[] { '/' }, 2).Select(variable => variable.Trim()).ToArray();
                        if (data.Length == 2)
                        {
                            delimiter = '/';
                        }
                        else
                        {
                            data = values.Split(new char[] { '%' }, 2).Select(variable => variable.Trim()).ToArray();
                            if (data.Length == 2)
                            {
                                delimiter = '%';
                            }
                        }
                    }
                }
            }

            if (data.Length == 2)
            {
                //Loops through the variable list and sets the appropriate value of variables
                for (int index = 0; index < data.Length; index++)
                {
                    foreach (Variable variable in variableList)
                    {
                        if (data[index] == variable.Name)
                        {
                            data[index] = variable.Values;
                        }
                    }
                }

                if (!int.TryParse(data[0], out _) || !int.TryParse(data[1], out _))
                {
                    if (regex.IsMatch(data[1]))
                    {
                        throw new UnsupportedCalculationException("Unsupported calculation at line " + variableNumber);
                    }
                    else
                    {
                        if (data[0] != "" && data[1] != "")
                        {
                            throw new UndefinedVariableException("Undefined variable at line " + variableNumber);
                        }
                        else
                        {
                            throw new FormatException("Input string was not in a correct format at line " + variableNumber);
                        }
                    }
                }

                switch (delimiter)
                {
                    case '+':
                        values = (int.Parse(data[0]) + int.Parse(data[1])).ToString();
                        break;

                    case '-':
                        values = (int.Parse(data[0]) - int.Parse(data[1])).ToString();
                        break;

                    case '*':
                        values = (int.Parse(data[0]) * int.Parse(data[1])).ToString();
                        break;

                    case '/':
                        if (data[1] == "0")
                        {
                            throw new DivideByZeroException("Attempted to divide by zero at line " + variableNumber);
                        }
                        values = (int.Parse(data[0]) / int.Parse(data[1])).ToString();
                        break;

                    case '%':
                        values = (int.Parse(data[0]) % int.Parse(data[1])).ToString();
                        break;
                }
            }
            else
            {
                foreach (Variable variable in variableList)
                {
                    if (values == variable.Name)
                    {
                        values = variable.Values;
                    }
                }
            }

            if (!int.TryParse(values, out _))
            {
                if (values == "")
                {
                    throw new EmptyDeclarationException("Empty declaration at line " + variableNumber);
                }
                else
                {
                    if (regex.IsMatch(values))
                    {
                        throw new UnsupportedCalculationException("Undefined operator at line" + variableNumber);
                    }
                    else
                    {
                        throw new UndefinedVariableException("Undefined variable at line " + variableNumber);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Represents user-defined exception for undefined variable.
    /// </summary>
    public class UndefinedVariableException : Exception
    {
        /// <summary>
        /// Default constructor to pass message.
        /// </summary>
        /// <param name="message">Exception Message</param>
        public UndefinedVariableException(string message) : base(message)
        { }
    }

    /// <summary>
    /// Represents user-defined exception for unsupported calculation.
    /// </summary>
    public class UnsupportedCalculationException : Exception
    {
        /// <summary>
        /// Default constructor to pass message.
        /// </summary>
        /// <param name="message">Exception Message</param>
        public UnsupportedCalculationException(string message) : base(message)
        { }
    }

    /// <summary>
    /// Represents user-defined exception for empty variable declaration.
    /// </summary>
    public class EmptyDeclarationException : Exception
    {
        /// <summary>
        /// Default constructor to pass message.
        /// </summary>
        /// <param name="message">Exception Message</param>
        public EmptyDeclarationException(string message) : base(message)
        { }
    }
}