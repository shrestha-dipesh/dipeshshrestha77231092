﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the center point of the drawing area.
    /// </summary>
    public class CenterPoint
    {
        private static int x_coordinate = 0;
        private static int y_coordinate = 0;

        /// <summary>
        /// x_coordinate Property
        /// </summary>
        public static int X_Coordinate
        {
            get { return x_coordinate; }
            set { x_coordinate = value; }
        }

        /// <summary>
        /// y_coordinate Property
        /// </summary>
        public static int Y_Coordinate
        {
            get { return y_coordinate; }
            set { y_coordinate = value; }
        }

        /// <summary>
        /// Set the center point of the drawing area to the given coordinates with visual representation.
        /// </summary>
        /// <param name="pictureBox">Reference to the outputBox for creating the graphics</param>
        /// <param name="x_coordinate">X-Coordinate of the Center Point</param>
        /// <param name="y_coordinate">Y-Coordinate of the Center Point</param>
        public static void moveCenterPoint(PictureBox pictureBox, int x_coordinate, int y_coordinate)
        {
            X_Coordinate = x_coordinate;
            Y_Coordinate = y_coordinate;

            Graphics graphics = pictureBox.CreateGraphics();
            SolidBrush brush = new SolidBrush(Color.Red);

            graphics.FillEllipse(brush, X_Coordinate - 4, Y_Coordinate - 4, 8, 8);

            brush.Dispose();
        }

        /// <summary>
        /// Fade the previous center point when new center point is defined.
        /// </summary>
        /// <param name="pictureBox">Reference to the outputBox for creating the graphics</param>
        public static void fadeCenterPoint(PictureBox pictureBox)
        {
            Graphics graphics = pictureBox.CreateGraphics();
            SolidBrush brush = new SolidBrush(Color.Gray);

            graphics.FillEllipse(brush, X_Coordinate - 4, Y_Coordinate - 4, 8, 8);

            brush.Dispose();
        }
    }
}