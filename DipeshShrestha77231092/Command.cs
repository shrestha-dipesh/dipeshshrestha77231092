﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the Command.
    /// </summary>
    public class Command : Commands
    {
        private string commandName;
        private string[] arguments;
        private string[] tempArguments;
        private string[] methodArguments;
        private int commandNumber;
        private static bool hasError = false;
        private string errorMessage = null;
        private static Color colour = Color.White;
        private readonly List<Variable> variableList = Variable.getVariables();
        private ConditionalCommand conditionalCommandObj;
        private IteratorCommand iteratorCommandObj;
        private MethodCommand methodCommandObj;
        private bool isInsideMethod = false;
        private static string[] currentFlashColor;
        private static bool currentFill;
        private bool conditionalFlag;
        private static int size = 1;

        /// <summary>
        /// Parameterized constructor to set the command name and argument.
        /// </summary>
        /// <param name="commandName">Name of the command</param>
        /// <param name="arguments">Parameters of the command</param>
        /// <param name="commandNumber">Line number of command</param>
        public Command(string commandName, string[] arguments, int commandNumber)
        {
            this.commandName = commandName;
            this.arguments = arguments;
            this.commandNumber = commandNumber;
        }

        /// <summary>
        /// commandName Property
        /// </summary>
        public string CommandName
        {
            get { return commandName; }
            set { commandName = value; }
        }

        /// <summary>
        /// arguments Property
        /// </summary>
        public string[] Arguments
        {
            get { return arguments; }
            set { arguments = value; }
        }

        /// <summary>
        /// hasError Property
        /// </summary>
        public static bool HasError
        {
            get { return hasError; }
            set { hasError = value; }
        }

        /// <summary>
        /// isInsideMethod Property
        /// </summary>
        public bool IsInsideMethod
        {
            get { return isInsideMethod; }
            set { isInsideMethod = value; }
        }

        /// <summary>
        /// conditionalCommandObj Property
        /// </summary>
        public ConditionalCommand ConditionalCommandObj
        {
            get { return conditionalCommandObj; }
            set { conditionalCommandObj = value; }
        }

        /// <summary>
        /// iteratorCommandObj Property
        /// </summary>
        public IteratorCommand IteratorCommandObj
        {
            get { return iteratorCommandObj; }
            set { iteratorCommandObj = value; }
        }

        /// <summary>
        /// methodCommandObj Property
        /// </summary>
        public MethodCommand MethodCommandObj
        {
            get { return methodCommandObj; }
            set { methodCommandObj = value; }
        }

        /// <summary>
        /// conditionalFlag Property
        /// </summary>
        public bool ConditionalFlag
        {
            get { return conditionalFlag; }
            set { conditionalFlag = value; }
        }

        /// <summary>
        /// Implemented method to check for syntax and runtime errors in the command.
        /// </summary>
        /// <returns>Error message</returns>
        public string debug()
        {
            //Conditional statement to check the command name and parameters
            if (commandName.ToLower() == "moveto")
            {
                if (arguments.Length != 2)
                {
                    hasError = true;
                    errorMessage = "Error: \"" + commandName + "\" requires 2 parameters at line " + commandNumber;
                }
            }
            else if (commandName.ToLower() == "circle" || commandName.ToLower() == "pen" || commandName.ToLower() == "fill")
            {
                if (arguments.Length != 1)
                {
                    hasError = true;
                    errorMessage = "Error: \"" + commandName + "\" requires 1 parameter at line " + commandNumber;
                }

                if (commandName.ToLower() == "pen")
                {
                    if (!(arguments[0].ToLower() == "redgreen" || arguments[0].ToLower() == "blueyellow" || arguments[0].ToLower() == "blackwhite"))
                    {
                        ColorRepository colorRepository = new ColorRepository();

                        //Using the Iterator Design Pattern to check if the input color is supported
                        for (Iterator iterator = colorRepository.getIterator(); iterator.hasNext();)
                        {
                            string supportedColour = (string)iterator.next();
                            if (arguments[0].ToLower() == supportedColour)
                            {
                                hasError = false;
                                Shape.FlashOn = false;
                                break;
                            }
                            else
                            {
                                hasError = true;
                            }
                        }
                    }

                    if (hasError)
                    {
                        errorMessage = "Error: Unsupported color at line " + commandNumber;
                    }
                }

                if (commandName.ToLower() == "fill")
                {
                    if (!(arguments[0].ToLower() == "on" || arguments[0].ToLower() == "off"))
                    {
                        hasError = true;
                        errorMessage = "Error: Invalid parameter for fill at line " + commandNumber;

                    }
                }
            }
            else if (commandName.ToLower() == "rectangle" || commandName.ToLower() == "triangle" || commandName.ToLower() == "drawto")
            {
                if (arguments.Length != 2)
                {
                    hasError = true;
                    errorMessage = "Error: \"" + commandName + "\" requires 2 parameters at line " + commandNumber;
                }
            }
            else if (commandName.ToLower() == "polygon")
            {
                if (arguments.Length % 2 != 0)
                {
                    hasError = true;
                    errorMessage = "Error: Polygon is missing point at line " + commandNumber;
                }
            }
            else if (commandName.ToLower() == "var")
            {
                if (arguments.Length != 2)
                {
                    hasError = true;
                }
            }
            else if (commandName.ToLower() == "transform")
            {
                if (arguments.Length == 1)
                {
                    //Split the argument of transform command and checks if the argument is valid
                    string[] values = arguments[0].Split(new char[] { 'x' }, 2).Select(value => value.Trim()).ToArray();
                    if (values.Length > 2 || values[1] != "" || !int.TryParse(values[0], out _))
                    {
                        hasError = true;
                        errorMessage = "Error: Invalid parameter in \"" + commandName + "\" at line " + commandNumber;
                    }
                    else
                    {
                        arguments[0] = values[0];
                    }
                }
                else
                {
                    hasError = true;
                    errorMessage = "Error: \"" + commandName + "\" requires 1 parameter at line " + commandNumber;
                }
            }
            else if (!(commandName.ToLower() == "if" || commandName.ToLower() == "while" || commandName.ToLower() == "method"))
            {
                bool methodFound = false;
                bool variableFound = false;

                /*
                 * Checking to see if the method already exists
                 * Using Exception to catch syntax errors
                 */
                foreach (MethodCommand methodCommand in MethodCommand.getMethods())
                {
                    if (commandName == methodCommand.MethodName)
                    {
                        methodFound = true;

                        //Splits the arguments of the method and checks if the argument number is valid
                        methodArguments = arguments[0].Split(',').Select(parameter => parameter.Trim(new char[] { ' ', ')' })).ToArray();
                        if (methodCommand.Parameters.Count > 0)
                        {
                            if (methodArguments.Length == 1 && methodArguments[0] == "")
                            {
                                throw new Exception("Missing parameter(s) at line " + commandNumber);
                            }
                            else
                            {
                                foreach (string methodParameter in methodArguments)
                                {
                                    if (methodParameter == "")
                                    {
                                        throw new Exception("Invalid parameter format at line " + commandNumber);
                                    }
                                }
                                if (methodCommand.Parameters.Count != methodArguments.Length)
                                {
                                    throw new Exception("Invalid number of parameter(s) at line " + commandNumber);
                                }
                            }
                        }
                        else
                        {
                            if (methodArguments.Length == 1)
                            {
                                if (methodArguments[0] != "")
                                {
                                    throw new Exception("Invalid number of parameter(s) at line " + commandNumber);
                                }
                            }
                            else
                            {
                                foreach (string methodParameter in methodArguments)
                                {
                                    if (methodParameter == "")
                                    {
                                        throw new Exception("Invalid parameter format at line " + commandNumber);
                                    }
                                }
                                if (methodCommand.Parameters.Count != methodArguments.Length)
                                {
                                    throw new Exception("Invalid number of parameter(s) at line " + commandNumber);
                                }
                            }
                        }
                        break;
                    }
                }

                //Loops through the variable list to check if the command name is a valid variable
                foreach (Variable variable in Variable.getVariables())
                {
                    if (commandName == variable.Name)
                    {
                        variableFound = true;
                        break;
                    }
                }

                if (!methodFound && !variableFound)
                {
                    hasError = true;
                    errorMessage = "Error: Unknown command \"" + commandName + "\" at line " + commandNumber;
                }
            }

            //Conditional statement to check for invalid format parameters
            if (commandName.ToLower() == "moveto" || commandName.ToLower() == "circle" || commandName.ToLower() == "rectangle" || commandName.ToLower() == "triangle" || commandName.ToLower() == "drawto" || commandName.ToLower() == "polygon")
            {
                //Storing the value of arguments in temporary array to make changes without affecting the arguments
                tempArguments = new string[arguments.Length];
                for (int index = 0; index < arguments.Length; index++)
                {
                    tempArguments[index] = arguments[index];
                }

                for (int index = 0; index < tempArguments.Length; index++)
                {
                    foreach (Variable variable in variableList)
                    {
                        if (variable.Name == tempArguments[index])
                        {
                            tempArguments[index] = variable.Values;
                            break;
                        }
                    }
                }

                /*
                 * Checks to see if the variable is passed as argument in method
                 * Find the value of the variable from the dictionary of arguments
                 */
                if (isInsideMethod)
                {
                    for (int index = 0; index < tempArguments.Length; index++)
                    {
                        foreach (KeyValuePair<string, string> data in methodCommandObj.Parameters)
                        {
                            if (data.Key == tempArguments[index])
                            {
                                tempArguments[index] = data.Value;
                                break;
                            }
                        }
                    }
                }
                
                foreach (string argument in tempArguments)
                {
                    try
                    {
                        int.Parse(argument);
                    }
                    catch (FormatException)
                    {
                        hasError = true;
                        errorMessage = "Error: Invalid parameter in \"" + commandName + "\" at line " + commandNumber;
                    }
                }
            }
            return errorMessage;
        }

        /// <summary>
        /// Implemented method to execute the command.
        /// </summary>
        /// <param name="shape">Shape to be drawn</param>
        /// <param name="pictureBox">Reference to the outputBox for creating the graphics</param>
        public void execute(Shape shape, PictureBox pictureBox)
        {
            if (!hasError)
            {
                if (commandName.ToLower() == "moveto")
                {
                    CenterPoint.fadeCenterPoint(pictureBox);
                    CenterPoint.moveCenterPoint(pictureBox, int.Parse(tempArguments[0]), int.Parse(tempArguments[1]));
                }
                else if (commandName.ToLower() == "pen")
                {
                    if (arguments[0].ToLower() == "redgreen")
                    {
                        Shape.FlashOn = true;
                        currentFlashColor = new string[] { "red", "green" };
                    }  
                    else if (arguments[0].ToLower() == "blueyellow")
                    {
                        Shape.FlashOn = true;
                        currentFlashColor = new string[] { "blue", "yellow" };
                    }
                    else if (arguments[0].ToLower() == "blackwhite")
                    {
                        Shape.FlashOn = true;
                        currentFlashColor = new string[] { "black", "white" };
                    }
                    else
                    {
                        colour = Color.FromName(arguments[0]);
                    }
                }
                else if (commandName.ToLower() == "fill")
                {
                    if (arguments[0].ToLower() == "on")
                    {
                        currentFill = true;
                    }
                    else if (arguments[0].ToLower() == "off")
                    {
                        currentFill = false;
                    }
                }
                else if (commandName.ToLower() == "var")
                {
                    Variable.add(new Variable(arguments[0], arguments[1], commandNumber));
                }
                else if (commandName.ToLower() == "if")
                {
                    bool commandValid = conditionalCommandObj.checkCondition(arguments[0]);
                    conditionalCommandObj.executeCommand(commandValid);
                }
                else if (commandName.ToLower() == "while")
                {
                    bool commandValid = iteratorCommandObj.checkCondition(arguments[0]);
                    int count = 0;

                    //Looping through the commands until the condition is invalid
                    while (commandValid)
                    {
                        count++;
                        if (HasError)
                        {
                            break;
                        }
                        else if (count == 100)
                        {
                            throw new StackOverflowException();
                        }
                        IteratorCommandObj.executeCommand();
                        commandValid = iteratorCommandObj.checkCondition(arguments[0]);
                    }
                }
                else if (commandName.ToLower() == "transform")
                {
                    size = int.Parse(arguments[0]);

                }
                else
                {
                    bool methodFound = false;
                    bool variableFound = false;

                    /*
                     * Finds the method from the list when method is called
                     * Passes the value from the method call to method arguments
                     * Execute the commands inside method
                     */
                    foreach (MethodCommand methodCommand in MethodCommand.getMethods())
                    {
                        if (commandName == methodCommand.MethodName)
                        {
                            for (int index = 0; index < methodCommand.Parameters.Count; index++)
                            {
                                KeyValuePair<string, string> methodParameter = methodCommand.Parameters.ElementAt(index);
                                for (int counter = 0; counter < methodArguments.Length; counter++)
                                {
                                    foreach (Variable variable in Variable.getVariables())
                                    {
                                        if (methodArguments[counter] == variable.Name)
                                        {
                                            methodArguments[counter] = variable.Values;
                                            break;
                                        }
                                    }
                                }
                                methodCommand.Parameters[methodParameter.Key] = methodArguments[index];
                            }
                            methodCommand.executeCommand();
                            methodFound = true;
                            break;
                        }
                    }

                    //Checks to see if the variable is global (not inside method)
                    if (!methodFound)
                    {
                        List<Variable> variableList = Variable.getVariables();
                        string[] value = arguments[0].Split('=').Select(parameter => parameter.Trim()).ToArray();

                        //Loops through the variable list and computes the value
                        for (int index = 0; index < variableList.Count; index++)
                        {
                            if (commandName == variableList[index].Name)
                            {
                                Variable tempVariable = new Variable("", value[1], commandNumber);
                                tempVariable.computeValue();
                                variableList[index].Values = tempVariable.Values;
                                variableList[index].VariableNumber = commandNumber;
                                variableFound = true;
                            }
                        }
                    }

                    if (!methodFound && !variableFound)
                    {
                        shape.FillOn = currentFill;
                        
                        if (Shape.FlashOn)
                        {
                            shape.setFlashColor(currentFlashColor[0], currentFlashColor[1]);
                        }

                        //Sets the size of the shapes from transform command
                        for (int index = 0; index < tempArguments.Length; index++)
                        {
                            tempArguments[index] = (size * int.Parse(tempArguments[index])).ToString();
                        }

                        if (commandName.ToLower() == "circle")
                        {
                            shape.set(colour, CenterPoint.X_Coordinate, CenterPoint.Y_Coordinate, int.Parse(tempArguments[0]));
                        }
                        else if (commandName.ToLower() == "polygon")
                        {
                            List<int> parameterList = new List<int>() { CenterPoint.X_Coordinate, CenterPoint.Y_Coordinate };
                            foreach (string argument in tempArguments)
                            {
                                parameterList.Add(int.Parse(argument));
                            }

                            shape.set(colour, parameterList.ToArray());
                        }
                        else
                        {
                            shape.set(colour, CenterPoint.X_Coordinate, CenterPoint.Y_Coordinate, int.Parse(tempArguments[0]), int.Parse(tempArguments[1]));
                        }

                        Graphics graphics = pictureBox.CreateGraphics();
                        shape.draw(graphics);
                    }
                }
            }
        }
    }
}