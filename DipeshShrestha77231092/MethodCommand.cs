﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents method commands that is stored inside method block.
    /// </summary>
    public class MethodCommand
    {
        private string methodName;
        private Dictionary<string, string> parameters = new Dictionary<string, string>();
        private static List<MethodCommand> methodCommandList;
        private List<Command> commandList;
        private static PictureBox outputBox;
        private static Label statusBox;
        private int lineNumber;
        ShapeFactory factory = new ShapeFactory();
        Shape shape;

        /// <summary>
        /// outputBox Property
        /// </summary>
        public static PictureBox OutputBox
        {
            get { return outputBox; }
            set { outputBox = value; }
        }

        /// <summary>
        /// statusBox Property
        /// </summary>
        public static Label StatusBox
        {
            get { return statusBox; }
            set { statusBox = value; }
        }

        /// <summary>
        /// methodName Property
        /// </summary>
        public string MethodName
        {
            get { return methodName; }
            set { methodName = value; }
        }

        /// <summary>
        /// parameters Property
        /// </summary>
        public Dictionary<string, string> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        /// <summary>
        /// Default constructor to instantiate the method and command list.
        /// </summary>
        public MethodCommand()
        {
            methodCommandList = new List<MethodCommand>();
        }

        /// <summary>
        /// Parameterized constructor to set the method name and line number of the commands.
        /// </summary>
        /// <param name="methodName">Name of the method block</param>
        /// <param name="lineNumber">Line number of the command</param>
        public MethodCommand(string methodName, int lineNumber)
        {
            this.methodName = methodName;
            this.lineNumber = lineNumber;
            commandList = new List<Command>();
        }

        /// <summary>
        /// Method to add command to the list.
        /// </summary>
        /// <param name="command">Object of command to be executed</param>
        public void addCommand(Command command)
        {
            commandList.Add(command);
        }

        /// <summary>
        /// Method to add method block to the list using Composite Design Pattern.
        /// </summary>
        /// <param name="methodCommand">Object of the method block</param>
        public static void addMethod(MethodCommand methodCommand)
        {
            bool methodFound = false;
            if (methodCommandList.Count == 0)
            {
                methodCommandList.Add(methodCommand);
            }
            else
            {
                foreach (MethodCommand localMethodCommand in methodCommandList)
                {
                    if (localMethodCommand.methodName == methodCommand.methodName)
                    {
                        methodFound = true;
                        throw new Exception("Method \"" + methodCommand.methodName + "\" already exists");
                    }
                }
                if (!methodFound)
                {
                    methodCommandList.Add(methodCommand);
                }
            }
        }

        /// <summary>
        /// Method to return the list of method commands.
        /// </summary>
        /// <returns>Object reference of the method commands</returns>
        public static List<MethodCommand> getMethods()
        {
            return methodCommandList;
        }

        /// <summary>
        /// Method to execute all the commands in the command list.
        /// </summary>
        public void executeCommand()
        {
            foreach (Command command in commandList)
            {
                try
                {
                    //Call the getShape method of ShapeFactory to return the appropriate shape object
                    shape = factory.getShape(command.CommandName);
                }
                catch (ArgumentException) { }

                string errorMessage = command.debug();
                if (errorMessage != null)
                {
                    statusBox.Text = errorMessage;
                    break;
                }

                try
                {
                    command.execute(shape, outputBox);
                }
                catch (FormatException)
                {
                    Command.HasError = true;
                    statusBox.Text = "Error: Invalid parameter in \"" + command.CommandName + "\" ";
                }
            }
        }
    }
}