﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the interface of command to enforce abstract methods.
    /// </summary>
    interface Commands
    {
        /// <summary>
        /// Abstract method to check for syntax and runtime errors.
        /// </summary>
        /// <returns>Error message</returns>
        string debug();

        /// <summary>
        /// Abstract method to execute the command.
        /// </summary>
        /// <param name="shape">Shape to be drawn</param>
        /// <param name="picturebox">Reference to the outputBox for creating the graphics</param>
        void execute(Shape shape, PictureBox picturebox);
    }
}