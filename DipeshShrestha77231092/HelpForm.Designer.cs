﻿namespace DipeshShrestha77231092
{
    partial class HelpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpForm));
            this.label1 = new System.Windows.Forms.Label();
            this.movetoPicture = new System.Windows.Forms.PictureBox();
            this.drawtoPicture = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.circlePicture = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rectanglePicture = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.trianglePicture = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.penPicture = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.fillPicture = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.movetoPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.drawtoPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.circlePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectanglePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trianglePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.penPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fillPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Crimson;
            this.label1.Location = new System.Drawing.Point(17, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(452, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Welcome to the Shapes GUI guideline.";
            // 
            // movetoPicture
            // 
            this.movetoPicture.Location = new System.Drawing.Point(71, 142);
            this.movetoPicture.Name = "movetoPicture";
            this.movetoPicture.Size = new System.Drawing.Size(109, 92);
            this.movetoPicture.TabIndex = 0;
            this.movetoPicture.TabStop = false;
            this.movetoPicture.Paint += new System.Windows.Forms.PaintEventHandler(this.movetoPicture_Paint);
            // 
            // drawtoPicture
            // 
            this.drawtoPicture.Location = new System.Drawing.Point(71, 254);
            this.drawtoPicture.Name = "drawtoPicture";
            this.drawtoPicture.Size = new System.Drawing.Size(109, 92);
            this.drawtoPicture.TabIndex = 0;
            this.drawtoPicture.TabStop = false;
            this.drawtoPicture.Paint += new System.Windows.Forms.PaintEventHandler(this.drawtoPicture_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(372, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(291, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "moveTo <x-coordinate> , <y-coordinate>";
            // 
            // circlePicture
            // 
            this.circlePicture.Location = new System.Drawing.Point(71, 365);
            this.circlePicture.Name = "circlePicture";
            this.circlePicture.Size = new System.Drawing.Size(109, 92);
            this.circlePicture.TabIndex = 0;
            this.circlePicture.TabStop = false;
            this.circlePicture.Paint += new System.Windows.Forms.PaintEventHandler(this.circlePicture_Paint);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(221, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "Command:";
            // 
            // rectanglePicture
            // 
            this.rectanglePicture.Location = new System.Drawing.Point(71, 475);
            this.rectanglePicture.Name = "rectanglePicture";
            this.rectanglePicture.Size = new System.Drawing.Size(109, 92);
            this.rectanglePicture.TabIndex = 0;
            this.rectanglePicture.TabStop = false;
            this.rectanglePicture.Paint += new System.Windows.Forms.PaintEventHandler(this.rectanglePicture_Paint);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(372, 273);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(287, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "drawTo <x-coordinate> , <y-coordinate>";
            // 
            // trianglePicture
            // 
            this.trianglePicture.Location = new System.Drawing.Point(71, 585);
            this.trianglePicture.Name = "trianglePicture";
            this.trianglePicture.Size = new System.Drawing.Size(109, 92);
            this.trianglePicture.TabIndex = 0;
            this.trianglePicture.TabStop = false;
            this.trianglePicture.Paint += new System.Windows.Forms.PaintEventHandler(this.trianglePicture_Paint);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(372, 384);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "circle <radius>";
            // 
            // penPicture
            // 
            this.penPicture.Location = new System.Drawing.Point(71, 697);
            this.penPicture.Name = "penPicture";
            this.penPicture.Size = new System.Drawing.Size(109, 92);
            this.penPicture.TabIndex = 0;
            this.penPicture.TabStop = false;
            this.penPicture.Paint += new System.Windows.Forms.PaintEventHandler(this.penPicture_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(221, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Description:";
            // 
            // fillPicture
            // 
            this.fillPicture.Location = new System.Drawing.Point(71, 811);
            this.fillPicture.Name = "fillPicture";
            this.fillPicture.Size = new System.Drawing.Size(109, 92);
            this.fillPicture.TabIndex = 0;
            this.fillPicture.TabStop = false;
            this.fillPicture.Paint += new System.Windows.Forms.PaintEventHandler(this.fillPicture_Paint);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(372, 494);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(208, 20);
            this.label14.TabIndex = 2;
            this.label14.Text = "rectangle <width> , <height>";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(221, 273);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "Command:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(372, 604);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(192, 20);
            this.label18.TabIndex = 2;
            this.label18.Text = "triangle <height> , <base>";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(372, 701);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 20);
            this.label22.TabIndex = 2;
            this.label22.Text = "pen <color>";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(221, 384);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(94, 20);
            this.label11.TabIndex = 2;
            this.label11.Text = "Command:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(372, 830);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(112, 20);
            this.label26.TabIndex = 2;
            this.label26.Text = "fill <on> / <off>";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(372, 191);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(646, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "This command moves the centre point from current coordinates to the passed coordi" +
    "nates.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(221, 494);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 20);
            this.label15.TabIndex = 2;
            this.label15.Text = "Command:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(221, 303);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 20);
            this.label8.TabIndex = 2;
            this.label8.Text = "Description:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(221, 604);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 20);
            this.label19.TabIndex = 2;
            this.label19.Text = "Command:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(221, 701);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(94, 20);
            this.label23.TabIndex = 2;
            this.label23.Text = "Command:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(221, 830);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 20);
            this.label27.TabIndex = 2;
            this.label27.Text = "Command:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(221, 414);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(105, 20);
            this.label12.TabIndex = 2;
            this.label12.Text = "Description:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(372, 303);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(542, 20);
            this.label9.TabIndex = 2;
            this.label9.Text = "This command draws a line from the centre point to the passed coordinates.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(221, 524);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 20);
            this.label16.TabIndex = 2;
            this.label16.Text = "Description:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(372, 414);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(486, 20);
            this.label13.TabIndex = 2;
            this.label13.Text = "This command draws a circle from the centre point with given radius.";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(221, 634);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(105, 20);
            this.label20.TabIndex = 2;
            this.label20.Text = "Description:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(221, 731);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 20);
            this.label24.TabIndex = 2;
            this.label24.Text = "Description:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(221, 860);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(105, 20);
            this.label28.TabIndex = 2;
            this.label28.Text = "Description:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(372, 524);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(589, 20);
            this.label17.TabIndex = 2;
            this.label17.Text = "This command draws a rectangle from the centre point with given width and height." +
    "";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(372, 634);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(573, 20);
            this.label21.TabIndex = 2;
            this.label21.Text = "This command draws a triangle from the centre point with given height and base.";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(372, 731);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(671, 20);
            this.label25.TabIndex = 2;
            this.label25.Text = "This command changes the color of the pen so the next shape to be drawn will have" +
    " that color.";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(372, 860);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(349, 20);
            this.label29.TabIndex = 2;
            this.label29.Text = "This command toggles the fill for the next shape.";
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label30.Location = new System.Drawing.Point(20, 53);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(1029, 45);
            this.label30.TabIndex = 1;
            this.label30.Text = resources.GetString("label30.Text");
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label31.Location = new System.Drawing.Point(15, 104);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(486, 20);
            this.label31.TabIndex = 1;
            this.label31.Text = " These are some of the codes which can be executed in both boxes:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label32.Location = new System.Drawing.Point(15, 925);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(509, 20);
            this.label32.TabIndex = 1;
            this.label32.Text = " These are some of the codes which are exclusive to the command box:";
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(65, 957);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(95, 23);
            this.label33.TabIndex = 3;
            this.label33.Text = "Command:";
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(65, 988);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(95, 23);
            this.label34.TabIndex = 3;
            this.label34.Text = "Command:";
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(65, 1017);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(95, 23);
            this.label35.TabIndex = 3;
            this.label35.Text = "Command:";
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(155, 957);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(95, 23);
            this.label36.TabIndex = 3;
            this.label36.Text = "Run";
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(155, 988);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(95, 23);
            this.label37.TabIndex = 3;
            this.label37.Text = "Clear";
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(155, 1017);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(95, 23);
            this.label38.TabIndex = 3;
            this.label38.Text = "Reset";
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(269, 957);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(106, 23);
            this.label39.TabIndex = 3;
            this.label39.Text = "Description:";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(372, 957);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(440, 23);
            this.label40.TabIndex = 3;
            this.label40.Text = "This command runs the code present in the program box.";
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(372, 988);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(299, 23);
            this.label41.TabIndex = 3;
            this.label41.Text = "This command clears the drawing area.";
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(372, 1017);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(394, 23);
            this.label42.TabIndex = 3;
            this.label42.Text = "This command moves the pen to the initial position.";
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(269, 988);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(106, 23);
            this.label43.TabIndex = 3;
            this.label43.Text = "Description:";
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(269, 1017);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(106, 23);
            this.label44.TabIndex = 3;
            this.label44.Text = "Description:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(221, 763);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(145, 20);
            this.label45.TabIndex = 2;
            this.label45.Text = "Supported Color:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.Color.White;
            this.label46.Location = new System.Drawing.Point(372, 763);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(529, 20);
            this.label46.TabIndex = 2;
            this.label46.Text = "Red, Green, Blue, White, Black, Yellow, Orange, Brown, Pink, Purple, Gray";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(555, 1036);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(0, 20);
            this.label48.TabIndex = 5;
            // 
            // HelpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1103, 690);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.fillPicture);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.penPicture);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.trianglePicture);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.rectanglePicture);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.circlePicture);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.drawtoPicture);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.movetoPicture);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HelpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Help";
            ((System.ComponentModel.ISupportInitialize)(this.movetoPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.drawtoPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.circlePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rectanglePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trianglePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.penPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fillPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox movetoPicture;
        private System.Windows.Forms.PictureBox drawtoPicture;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox circlePicture;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox rectanglePicture;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox trianglePicture;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox penPicture;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox fillPicture;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label48;
    }
}