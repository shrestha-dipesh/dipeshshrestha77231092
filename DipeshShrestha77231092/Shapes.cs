﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the interface of the shape to enforce abstract method.
    /// </summary>
    interface Shapes
    {
        /// <summary>
        /// Abstract method to set the required parameters.
        /// </summary>
        /// <param name="colour">Color of the shape</param>
        /// <param name="list">Array containing the required parameters</param>
        void set(Color colour, params int[] list);

        /// <summary>
        /// Abstract method to draw the shape on the graphics.
        /// </summary>
        /// <param name="graphics">Reference to the graphics where the shape is to be drawn</param>
        void draw(Graphics graphics);
    }
}
