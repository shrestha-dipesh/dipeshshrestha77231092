﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the interface of Iterator to enforce abstract methods.
    /// </summary>
    public interface Iterator
    {
        /// <summary>
        /// Abstract method to validate the index.
        /// </summary>
        /// <returns>True if valid</returns>
        bool hasNext();

        /// <summary>
        /// Abstract method to return next object from the list.
        /// </summary>
        /// <returns>Object of next item</returns>
        object next();
    }
}