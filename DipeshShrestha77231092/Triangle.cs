﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the Triangle shape.
    /// </summary>
    public class Triangle : Shape
    {
        private int height, bases;

        /// <summary>
        /// Default constructor to set the height and base of the triangle.
        /// </summary>
        public Triangle() : base()
        {
            height = 100;
            bases = 100;
        }

        /// <summary>
        /// Parameterized constructor to set the provided parameters to their respective attributes.
        /// </summary>
        /// <param name="colour">Color of the triangle</param>
        /// <param name="x_coordinate">X-Coordinate of the center point</param>
        /// <param name="y_coordinate">Y-Coordinate of the center point</param>
        /// <param name="height">Height of the triangle</param>
        /// <param name="bases">Base of the triangle</param>
        public Triangle(Color colour, int x_coordinate, int y_coordinate, int height, int bases) : base(colour, x_coordinate, y_coordinate)
        {
            this.height = height;
            this.bases = bases;
        }

        /// <summary>
        /// Overridden method of Shape to set the color, center point, height and base.
        /// </summary>
        /// <param name="colour">Color of the triangle</param>
        /// <param name="list">Array of parameters containing coordinates of center point, height and base</param>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            height = list[2];
            bases = list[3];
        }

        /// <summary>
        /// Overridden method of Shape to draw the triangle on the graphics.
        /// </summary>
        /// <param name="graphics">Reference to the graphics where the triangle is to be drawn</param>
        public override void draw(Graphics graphics)
        {
            //Start a thread that will flash alternate shape color
            if (FlashOn && !threadStarted)
            {
                newThread = new Thread(() => threadMethod(graphics));
                newThread.Start();
                addThread(newThread);
                isRunning = true;
                threadStarted = true;
            }
            else
            {
                //Use the brush to fill the triangle if fill command is used
                if (fillOn)
                {
                    SolidBrush brush = new SolidBrush(colour);
                    graphics.FillPolygon(brush, new Point[] { new Point(x_coordinate, y_coordinate - height / 2), new Point(x_coordinate + bases / 2, y_coordinate + height / 2), new Point(x_coordinate - bases / 2, y_coordinate + height / 2) });
                    brush.Dispose();
                }

                //Use the pen to draw the outline of the triangle is fill command is not used
                else
                {
                    Pen pen = new Pen(colour, 2);
                    graphics.DrawPolygon(pen, new Point[] { new Point(x_coordinate, y_coordinate - height / 2), new Point(x_coordinate + bases / 2, y_coordinate + height / 2), new Point(x_coordinate - bases / 2, y_coordinate + height / 2) });
                    pen.Dispose();
                }
            }
        }
    }
}