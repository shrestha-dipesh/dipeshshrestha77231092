﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represent the shape factory that returns the object of appropriate shape.
    /// </summary>
    public class ShapeFactory
    {
        /// <summary>
        /// Method that takes name of the shape and returns the appropriate shape.
        /// </summary>
        /// <param name="shapeType">Name of the shape</param>
        /// <returns>Object of the shape</returns>
        public Shape getShape(string shapeType)
        {
            shapeType = shapeType.ToLower().Trim();
            if (shapeType == "circle")
            {
                return new Circle();
            }
            else if (shapeType == "rectangle")
            {
                return new Rectangle();
            }
            else if (shapeType == "triangle")
            {
                return new Triangle();
            }
            else if (shapeType == "drawto")
            {
                return new Line();
            }
            else if (shapeType == "polygon")
            {
                return new Polygon();
            }
            else
            {
                ArgumentException argEx = new ArgumentException("Factory error: " + shapeType + " does not exist");
                throw argEx;
            }
        }
    }
}