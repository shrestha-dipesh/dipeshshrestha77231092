﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the interface of Container to enforce abstract methods.
    /// </summary>
    public interface Container
    {
        /// <summary>
        /// Abstract method to instantiate ColorIterator class.
        /// </summary>
        /// <returns>Object of the ColorIterator class.</returns>
        Iterator getIterator();
    }
}