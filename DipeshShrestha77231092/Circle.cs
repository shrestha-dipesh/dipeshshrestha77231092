﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the Circle shape.
    /// </summary>
    public class Circle : Shape
    {
        private int radius;
        private const double PI = Math.PI;
        
        /// <summary>
        /// Default constructor to set the radius of the circle.
        /// </summary>
        public Circle() : base()
        {
            radius = 100;
        }

        /// <summary>
        /// Parameterized constructor to set the provided parameters to their respective attributes. 
        /// </summary>
        /// <param name="colour">Color of the circle</param>
        /// <param name="x_coordinate">X-Coordinate of the center point</param>
        /// <param name="y_coordinate">Y-Coordinate of the center point</param>
        /// <param name="radius">Radius of the circle</param>
        public Circle(Color colour, int x_coordinate, int y_coordinate, int radius) : base(colour, x_coordinate, y_coordinate)
        {
            this.radius = radius;
        }

        /// <summary>
        /// Overridden method of Shape to set the color, center point and radius.
        /// </summary>
        /// <param name="colour">Color of the circle</param>
        /// <param name="list">Array of parameters containing coordinates of center point and radius</param>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            radius = list[2];
        }

        /// <summary>
        /// Overridden method of Shape to draw the circle on the graphics.
        /// </summary>
        /// <param name="graphics">Reference to the graphics where the circle is to be drawn</param>
        public override void draw(Graphics graphics)
        {
            //Start a thread that will flash alternate shape color
            if (FlashOn && !threadStarted)
            {
                newThread = new Thread(() => threadMethod(graphics));
                newThread.Start();
                addThread(newThread);
                isRunning = true;
                threadStarted = true;
            }
            else
            {
                //Use the brush to fill the circle if fill command is used
                if (fillOn)
                {
                    SolidBrush brush = new SolidBrush(colour);
                    graphics.FillEllipse(brush, x_coordinate - radius, y_coordinate - radius, radius * 2, radius * 2);
                    brush.Dispose();
                }

                //Use the pen to draw the outline of the circle is fill command is not used
                else
                {
                    Pen pen = new Pen(colour, 2);
                    graphics.DrawEllipse(pen, x_coordinate - radius, y_coordinate - radius, radius * 2, radius * 2);
                    pen.Dispose();
                }
            }
        }
    }
}