﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents Command Parser class to parse the commands.
    /// </summary>
    public class CommandParser
    {
        //List to store individual commands
        private List<Command> commands = new List<Command>();
        private int lineNumber = 0;
        private ConditionalCommand conditionalCommand;
        private bool isConditional = false;
        private bool ifStarted = false;
        private IteratorCommand iteratorCommand;
        private bool isIterator = false;
        private bool whileStarted = false;
        private MethodCommand methodCommand = new MethodCommand();
        private bool isMethod = false;
        private bool methodStarted = false;
        private bool conditionalFlag;

        /// <summary>
        /// Default Constructor to instantiate the variable class.
        /// </summary>
        public CommandParser()
        {
            new Variable();
        }
        
        /// <summary>
        /// Method to read the input string and parse the commands.
        /// </summary>
        /// <param name="inputText">Input text from the text box</param>
        /// <param name="statusBox">Reference to statusBox to display the error message</param>
        /// <returns>Array of commands</returns>
        public Command[] parse(string inputText, Label statusBox)
        {
            //Splits the text line by line and store it in inputCommands array
            string[] inputCommands = inputText.Split('\n').Select(command => command.Trim()).ToArray();

            //Loops through each command and store it in commands list by creating an object
            foreach (string command in inputCommands)
            {
                lineNumber++;
                try
                {
                    if (command != "")
                    {
                        //Splits each command by space to separate the command name and parameters
                        string[] input = command.Split(new char[] { ' ' }, 2);
                        if (input[0].ToLower() == "if")
                        {
                            Command ifCommand = new Command(input[0], new string[] { input[1] }, lineNumber);
                            commands.Add(ifCommand);
                            conditionalCommand = new ConditionalCommand(lineNumber);
                            ifCommand.ConditionalCommandObj = conditionalCommand;
                            isConditional = true;
                            ifStarted = true;
                            conditionalFlag = true;
                        }
                        else if (input[0].ToLower() == "else")
                        {
                            conditionalFlag = false;
                        }
                        else if (input[0].ToLower() == "endif")
                        {
                            isConditional = false;
                            if (!ifStarted)
                            {
                                throw new MissingKeywordException("Missing keyword \"if\"");
                            }
                        }
                        else if (input[0].ToLower() == "while")
                        {
                            Command whileCommand = new Command(input[0], new string[] { input[1] }, lineNumber);
                            commands.Add(whileCommand);
                            iteratorCommand = new IteratorCommand(lineNumber);
                            whileCommand.IteratorCommandObj = iteratorCommand;
                            isIterator = true;
                            whileStarted = true;
                        }
                        else if (input[0].ToLower() == "endwhile")
                        {
                            isIterator = false;
                            if (!whileStarted)
                            {
                                throw new MissingKeywordException("Missing keyword \"while\"");
                            }
                        }
                        else if (input[0].ToLower() == "method")
                        {
                            if (!(input[1].Contains('(') && input[1].Contains(')')))
                            {
                                throw new MissingKeywordException("Missing bracket(s) at line " + lineNumber);
                            }

                            //Splits the method name and parameters and store them in methodParameters array
                            string[] methodParameters = input[1].Split(new char[] { '(' }, 2).Select(parameter => parameter.Trim()).ToArray();
                            if (methodParameters[0] == "")
                            {
                                throw new MissingKeywordException("Missing method name at line " + lineNumber);
                            }

                            //Split the method parameters and store in the parameters array
                            string[] parameters = methodParameters[1].Split(',').Select(parameter => parameter.Trim(new char[] { ' ', ')' })).ToArray();
                            methodCommand = new MethodCommand(methodParameters[0], lineNumber);

                            foreach (string parameter in parameters)
                            {
                                if (parameter != "")
                                {
                                    methodCommand.Parameters.Add(parameter, "");
                                }
                            }
                            
                            MethodCommand.addMethod(methodCommand);
                            isMethod = true;
                            methodStarted = true;
                        }
                        else if (input[0].ToLower() == "endmethod")
                        {
                            isMethod = false;
                            if (!methodStarted)
                            {
                                throw new MissingKeywordException("Missing keyword \"method\"");
                            }
                        }
                        
                        else
                        {
                            string[] parameters = { };
                            if (input[0].ToLower() == "var")
                            {
                                parameters = input[1].Split(new char[] { '=' }, 2).Select(variable => variable.Trim()).ToArray();
                            }
                            else
                            {
                                //Checks to see if the command is a method call
                                if (command.Contains('(') && command.Contains(')'))
                                {
                                    string[] methodData = command.Split(new char[] { '(' }, 2).Select(data => data.Trim()).ToArray();
                                    if (methodData.Length == 2)
                                    {
                                        commands.Add(new Command(methodData[0], new string[] { methodData[1] }, lineNumber));
                                        continue;
                                    }
                                }
                                else
                                {
                                    //Splits the parameters by command and remove unnecessary preceding and succeeding white spaces
                                    parameters = input[1].Split(',').Select(parameter => parameter.Trim()).ToArray();
                                }
                            }
                            Command newCommand = new Command(input[0], parameters, lineNumber);
                            if (isConditional)
                            {
                                newCommand.ConditionalFlag = conditionalFlag;
                                conditionalCommand.add(newCommand);
                            }
                            else if (isIterator)
                            {
                                iteratorCommand.add(newCommand);
                            }
                            else if (isMethod)
                            {
                                methodCommand.addCommand(newCommand);
                                newCommand.MethodCommandObj = methodCommand;
                                newCommand.IsInsideMethod = true;
                            }
                            else
                            {
                                commands.Add(newCommand);
                            }
                        }
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    Command.HasError = true;
                    statusBox.Text = "Error: Invalid command format at line " + (Array.IndexOf(inputCommands, command) + 1);
                    break;
                }
            }

            if (isConditional)
            {
                throw new MissingKeywordException("Missing keyword \"endif\"");
            }

            if (isIterator)
            {
                throw new MissingKeywordException("Missing keyword \"endwhile\"");
            }

            if (isMethod)
            {
                throw new MissingKeywordException("Missing keyword \"endmethod\"");
            }

            return commands.ToArray();
        }
    }

    /// <summary>
    /// Represents user-defined exception for missing keyword.
    /// </summary>
    public class MissingKeywordException : Exception
    { 
        /// <summary>
        /// Default constructor to pass message.
        /// </summary>
        /// <param name="message">Exception Message</param>
        public MissingKeywordException(string message) : base (message)
        { }
    }
}