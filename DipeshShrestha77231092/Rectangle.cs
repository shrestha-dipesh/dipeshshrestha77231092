﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the Rectangle shape.
    /// </summary>
    public class Rectangle : Shape
    {
        private int width, height;

        /// <summary>
        /// Default constructor to set the width and height of the rectangle.
        /// </summary>
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }

        /// <summary>
        /// Parameterized constructor to set the provided parameters to their respective attributes.
        /// </summary>
        /// <param name="colour">Color of the rectangle</param>
        /// <param name="x_coordinate">X-Coordinate of the center point</param>
        /// <param name="y_coordinate">Y-Coordinate of the center point</param>
        /// <param name="width">Width of the rectangle</param>
        /// <param name="height">Height of the rectangle</param>
        public Rectangle(Color colour, int x_coordinate, int y_coordinate, int width, int height) : base(colour, x_coordinate, y_coordinate)
        {
            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// Overridden method of Shape to set the color, center point, width and height.
        /// </summary>
        /// <param name="colour">Color of the rectangle</param>
        /// <param name="list">Array of parameters containing coordinates of center point, width and height</param>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            width = list[2];
            height = list[3];
        }

        /// <summary>
        /// Overridden method of Shape to draw the rectangle on the graphics.
        /// </summary>
        /// <param name="graphics">Reference to the graphics where the rectangle is to be drawn</param>
        public override void draw(Graphics graphics)
        {
            //Start a thread that will flash alternate shape color
            if (FlashOn && !threadStarted)
            {
                newThread = new Thread(() => threadMethod(graphics));
                newThread.Start();
                addThread(newThread);
                isRunning = true;
                threadStarted = true;
            }
            else
            {
                //Use the brush to fill the rectangle if fill command is used
                if (fillOn)
                {
                    SolidBrush brush = new SolidBrush(colour);
                    graphics.FillRectangle(brush, x_coordinate - width / 2, y_coordinate - height / 2, width, height);
                    brush.Dispose();
                }

                //Use the pen to draw the outline of the rectangle is fill command is not used
                else
                {
                    Pen pen = new Pen(colour, 2);
                    graphics.DrawRectangle(pen, x_coordinate - width / 2, y_coordinate - height / 2, width, height);
                    pen.Dispose();
                }
            } 
        }
    }
}