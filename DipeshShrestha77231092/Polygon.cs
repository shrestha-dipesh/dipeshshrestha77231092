﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DipeshShrestha77231092
{
    /// <summary>
    /// Represents the Polygon shape
    /// </summary>
    public class Polygon : Shape
    {
        private int[] points;

        /// <summary>
        /// Default constructor to set the points of the polygon.
        /// </summary>
        public Polygon() : base()
        {
            points = new int[] { 0, 0 };
        }

        /// <summary>
        /// Parameterized constructor to set the provided parameters to their respective attributes.
        /// </summary>
        /// <param name="colour">Color of the polygon</param>
        /// <param name="x_coordinate">X-Coordinate of the center point</param>
        /// <param name="y_coordinate">Y-Coordinate of the center point</param>
        /// <param name="points">Points of the Polygon</param>
        public Polygon(Color colour, int x_coordinate, int y_coordinate, int[] points) : base(colour, x_coordinate, y_coordinate)
        {
            this.points = points;
        }

        /// <summary>
        /// Overridden method of Shape to set the color, center point and points.
        /// </summary>
        /// <param name="colour">Color of the rectangle</param>
        /// <param name="list">Array of parameters containing coordinates of center point and points</param>
        public override void set(Color colour, params int[] list)
        {
            base.set(colour, list[0], list[1]);
            points = list;
        }

        /// <summary>
        /// Overridden method of Shape to draw the polygon on the graphics.
        /// </summary>
        /// <param name="graphics">Reference to the graphics where the polygon is to be drawn</param>
        public override void draw(Graphics graphics)
        {
            //Start a thread that will flash alternate shape color
            if (FlashOn && !threadStarted)
            {
                newThread = new Thread(() => threadMethod(graphics));
                newThread.Start();
                addThread(newThread);
                isRunning = true;
                threadStarted = true;
            }
            else
            {
                List<PointF> pointList = new List<PointF>();
                PointF point;
                for (int index = 0; index < points.Length; index += 2)
                {
                    point = new Point(points[index], points[index + 1]);
                    pointList.Add(point);
                }

                //Use the brush to fill the rectangle if fill command is used
                if (fillOn)
                {
                    SolidBrush brush = new SolidBrush(colour);
                    graphics.FillPolygon(brush, pointList.ToArray());
                }

                //Use the pen to draw the outline of the rectangle is fill command is not used
                else
                {
                    Pen pen = new Pen(colour);
                    graphics.DrawPolygon(pen, pointList.ToArray());
                }
            }
            
        }
    }
}