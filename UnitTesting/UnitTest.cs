﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using DipeshShrestha77231092;
using System.Windows.Forms;

namespace UnitTesting
{
    /// <summary>
    /// Represents Unit test class to test valid and invalid shapes and commands.
    /// </summary>
    [TestClass]
    public class UnitTest
    {
        /// <summary>
        /// Default constructor to instantiate the variable and methodCommand class.
        /// </summary>
        public UnitTest()
        {
            new Variable();
            new MethodCommand();
        }

        /// <summary>
        /// Test method for ShapeFactory class.
        /// </summary>
        [TestMethod]
        public void testShapeFactory()
        {
            //Test shapes
            string shapeValid = "triangle";
            string shapeInvalid = "Octagon";

            //Object Instantiation
            ShapeFactory shapeFactory = new ShapeFactory();
            Shape expectedShape = new Triangle();
            Shape actualShape = shapeFactory.getShape(shapeValid);

            //Assert Tests
            ReferenceEquals(expectedShape, actualShape);
            Assert.ThrowsException<ArgumentException>(() => shapeFactory.getShape(shapeInvalid));
        }

        /// <summary>
        /// Test method for GuiForm class.
        /// </summary>
        [TestMethod]
        public void testRunProgram()
        {
            //Test commands
            string inputValid = "rectangle 20, 20";
            string inputInvalid = "";

            //Object Instantiation
            GuiForm guiForm = new GuiForm();

            //Assert test for valid command
            guiForm.runProgram(inputValid);
            Assert.IsFalse(Command.HasError);

            //Assert test for invalid command
            guiForm.runProgram(inputInvalid);
            Assert.IsTrue(Command.HasError);
        }

        /// <summary>
        /// Test method for CommandParser class.
        /// </summary>
        [TestMethod]
        public void testParseCommand()
        {
            //Test commands
            string commandValid = "rectangle 20, 20\ncircle 40";
            string commandInvalid = "moveTo100,100";
            
            //Object instantiation
            Label label = new Label();
            CommandParser parser = new CommandParser();

            //Assert test for invalid command
            parser.parse(commandInvalid, label);
            Assert.AreNotEqual(false, Command.HasError);

            //Assert test for valid command
            Command[] actualCommands = parser.parse(commandValid, label);
            Command command = new Command("rectangle", new string[] { "20", "20"}, 1);
            Command[] expectedCommands = { new Command("rectangle", new string[] { "20", "20" }, 1), new Command("circle", new string[] { "40" }, 1) };
            Assert.AreEqual(expectedCommands.ToString(), actualCommands.ToString());

            //Assert tests for missing keyword
            Assert.ThrowsException<MissingKeywordException>(() => parser.parse("while num < 10\ncircle num\nnum = num + 10", label));

            Assert.ThrowsException<MissingKeywordException>(() => parser.parse("circle 10\nendif", label));

            Assert.ThrowsException<MissingKeywordException>(() => parser.parse("method draw(num1, num2\ncircle 100\nendmethod", label));

            Assert.ThrowsException<MissingKeywordException>(() => parser.parse("method ()\nrectangle 100, 50\nendmethod", label));
        }

        /// <summary>
        /// Test method for Command class.
        /// </summary>
        [TestMethod]
        public void testCommands()
        {
            //Object and variable declaration
            string actualMessage;
            string expectedMessage;

            //Assert test for valid command
            Command commandValid = new Command("drawTo", new string[] { "100", "150" }, 1);
            actualMessage = commandValid.debug();
            Assert.IsNull(actualMessage);

            //Assert test for invalid parameter command
            Command parameterInvalid = new Command("triangle", new string[] { "50", "60", "70" }, 1);
            actualMessage = parameterInvalid.debug();
            expectedMessage = "Error: \"triangle\" requires 2 parameters at line 1";
            Assert.AreEqual(expectedMessage, actualMessage);

            //Assert test for unknown command
            Command commandUnknown = new Command("pun", new string[] { "red" }, 1);
            actualMessage = commandUnknown.debug();
            expectedMessage = "Error: Unknown command \"pun\" at line 1";
            Assert.AreEqual(expectedMessage, actualMessage);

            //Assert test for unsupported color command
            Command colorUnsupported = new Command("pen", new string[] { "violet" }, 1);
            actualMessage= colorUnsupported.debug();
            expectedMessage = "Error: Unsupported color at line 1";
            Assert.AreEqual(expectedMessage, actualMessage);

            //Assert test for invalid parameter command
            Command invalidParameter = new Command("fill", new string[] { "open" }, 1);
            actualMessage = invalidParameter.debug();
            expectedMessage = "Error: Invalid parameter for fill at line 1";
            Assert.AreEqual(expectedMessage, actualMessage);

            //Assert test for valid polygon command
            Command polygonValid = new Command("polygon", new string[] { "100", "150", "200", "150" }, 1);
            actualMessage = commandValid.debug();
            Assert.IsNull(actualMessage);

            //Assert test for missing points in polygon command
            Command pointMissing = new Command("polygon", new string[] { "10", "20", "30" }, 1);
            actualMessage = pointMissing.debug();
            expectedMessage = "Error: Polygon is missing point at line 1";
            Assert.AreEqual(expectedMessage, actualMessage);

            //Method instantiation and declaration
            MethodCommand method = new MethodCommand("add", 1); 
            MethodCommand.addMethod(method);
            method.Parameters.Add("num", "");

            //Assert test for missing parameters in method command
            Command parameterMissing = new Command("add", new string[] { ")" }, 2);
            Assert.ThrowsException<Exception>(() => parameterMissing.debug());

            //Assert test for invalid parameter format in method command
            Command formatInvalid = new Command("add", new string[] { "10,)" }, 2);
            Assert.ThrowsException<Exception>(() => formatInvalid.debug());

            //Assert test for invalid number of parameters in method command
            Command methodParameter = new Command("add", new string[] { "10, 20)" }, 2);
            Assert.ThrowsException<Exception>(() => methodParameter.debug());
        }

        /// <summary>
        /// Test method for Variable class.
        /// </summary>
        [TestMethod]
        public void testVariable()
        {
            //Variable declaration
            string expectedValue;
            string actualValue;

            //Assert test for valid variable instantiation
            Variable variableValid = new Variable("num", "10 + 10", 1);
            variableValid.computeValue();
            expectedValue = "20";
            actualValue = variableValid.Values;
            Assert.AreEqual(expectedValue, actualValue);

            //Assert test for duplicate variable declaration
            Variable oldVariable = new Variable("count", "45", 1);
            Variable newVariable = new Variable("count", "50", 2);
            Variable.add(oldVariable);
            Assert.ThrowsException<Exception>(() => Variable.add(newVariable));

            //Assert test for undefined variable operator
            Variable operatorUndefined;
            Assert.ThrowsException<Exception>(() => operatorUndefined = new Variable("num = ", "50", 1));

            //Assert test for unsupported variable calculation
            Variable calculationUnsupported;
            Assert.ThrowsException<UnsupportedCalculationException>(() => calculationUnsupported = new Variable("count", "50 + 30 * 2", 1));

            //Assert test for undefined variable
            Variable variableUndefined;
            Assert.ThrowsException<UndefinedVariableException>(() => variableUndefined = new Variable("count", "num + 10", 1));

            //Assert test for division by zero
            Variable zeroDivision;
            Assert.ThrowsException<DivideByZeroException>(() => zeroDivision = new Variable("point", "10 / 0", 1));

            //Assert test for empty variable declaration
            Variable variableEmpty;
            Assert.ThrowsException<EmptyDeclarationException>(() => variableEmpty = new Variable("point", "", 1));
        }

        /// <summary>
        /// Test method for ConditionalCommand class.
        /// </summary>
        [TestMethod]
        public void testConditional()
        {
            //Assert tests for valid conditional condition
            bool actualValue;
            ConditionalCommand condition = new ConditionalCommand(1);
            Variable.add(new Variable("index", "100", 1));
            actualValue = condition.checkCondition("index == 100");
            Assert.IsTrue(actualValue);

            actualValue = condition.checkCondition("index > 200");
            Assert.IsFalse(actualValue);

            //Assert test for undefined conditional operator
            Assert.ThrowsException<Exception>(() => condition.checkCondition("index =>= 20"));

            //Assert test for undefined conditional variable
            Assert.ThrowsException<Exception>(() => condition.checkCondition("index == newIndex"));
        }

        /// <summary>
        /// Test method for IteratorCommand class.
        /// </summary>
        [TestMethod]
        public void testIteration()
        {
            //Assert test for valid iteration condition
            bool actualValue;
            IteratorCommand iteration = new IteratorCommand(1);
            Variable.add(new Variable("counter", "10", 1));
            actualValue = iteration.checkCondition("counter < 100");
            Assert.IsTrue(actualValue);

            actualValue = iteration.checkCondition("counter >= 200");
            Assert.IsFalse(actualValue);

            //Assert test for undefined iteration operator
            CommandParser parser = new CommandParser();
            Assert.ThrowsException<Exception>(() => iteration.checkCondition("counter === 20"));

            //Assert test for undefined iteration variable
            Assert.ThrowsException<Exception>(() => iteration.checkCondition("counter < newCounter"));
        }

        /// <summary>
        /// Test method for MethodCommand class.
        /// </summary>
        [TestMethod]
        public void testMethod()
        {
            //Variable declaration
            string expectedMethodName;
            string actualMethodName;

            //Assert test for valid method
            MethodCommand methodCommand = new MethodCommand("add", 2);
            actualMethodName = methodCommand.MethodName;
            expectedMethodName = "add";
            Assert.AreEqual(expectedMethodName, actualMethodName);

            //Assert test for duplicate method declaration
            MethodCommand oldMethod = new MethodCommand("draw", 1);
            MethodCommand newMethod = new MethodCommand("draw", 2);
            MethodCommand.addMethod(oldMethod);
            Assert.ThrowsException<Exception>(() => MethodCommand.addMethod(newMethod));
        }
    }
}